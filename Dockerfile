FROM node:14.19.0-slim
EXPOSE 3001
WORKDIR /src
COPY package.json .
RUN npm install
COPY . .
CMD ["yarn", "dev"]