/* eslint-disable @typescript-eslint/no-explicit-any*/

import { Column, Entity } from 'typeorm';
import BaseEntity from '@root/src/utils/base.entity';

@Entity({ name: 'token', schema: 'user' })
export default class TokenEntity extends BaseEntity {
  @Column()
  accessToken: string;

  @Column()
  refreshToken: string;

  @Column()
  userId: number;

  @Column()
  hash: string;

  @Column({ type: 'json' })
  deviceData: any;
}
