import {
  DOMAIN,
  TOKEN_EXPIRES_IN,
  REFRESH_TOKEN_EXPIRES_IN,
  PRIVATE_KEY_PASSPHRASE,
  REFRESH_PRIVATE_KEY_PASSPHRASE,
  ACCESS_TOKEN_COOKIE_NAME,
  REFRESH_TOKEN_COOKIE_NAME,
  NODE_ENV
} from '@utils/config';
import { Response, Request } from 'express-serve-static-core';
import dayjs from 'dayjs';
import * as jwt from 'jsonwebtoken';
import path from 'path';
import * as fs from 'fs';
import MainDataSource from '../utils/main-data-source';
import TokenEntity from './entities/token.entity';
import { CookieOptions } from 'express';
import loggerHelper from '../utils/helpers/logger.helper';
import { VerifyOptions } from 'jsonwebtoken';
import { sendSMS } from '../utils/helpers/sms';

const privateKey = fs.readFileSync(path.resolve('./keys', 'private.pem'), 'utf8');
const publicKey = fs.readFileSync(path.resolve('./keys', 'public.pem'), 'utf8');

const refreshPrivateKey = fs.readFileSync(path.resolve('./keys', 'private-refresh.pem'), 'utf8');
const refreshPublicKey = fs.readFileSync(path.resolve('./keys', 'public-refresh.pem'), 'utf8');

export default class AuthService {
  private tokenRepo;

  constructor() {
    this.tokenRepo = MainDataSource.getRepository(TokenEntity);
  }

  private baseCookieOpts = {
    path: '/',
    secure: NODE_ENV === 'development' ? false : true,
    domain: DOMAIN.includes('localhost') ? '' : `.rent.com`
  };

  private deleteCookie(response: Response, valueName: string) {
    response.cookie(valueName, '', {
      httpOnly: true,
      signed: true,
      sameSite: 'strict',
      expires: new Date(),
      ...this.baseCookieOpts
    });
  }

  private HttpOnlyCookieOpts: CookieOptions = {
    ...this.baseCookieOpts,
    httpOnly: true
  };

  private userCookieOpts: CookieOptions = {
    ...this.baseCookieOpts,
    sameSite: 'strict'
  };

  private saveAccessTokenCookie(response: Response, accessToken: string) {
    response.cookie(ACCESS_TOKEN_COOKIE_NAME, accessToken, {
      ...this.HttpOnlyCookieOpts,
      expires: dayjs().add(TOKEN_EXPIRES_IN, 's').toDate()
    });
  }

  private saveRefreshTokenCookie(response: Response, refreshToken: string) {
    response.cookie(REFRESH_TOKEN_COOKIE_NAME, refreshToken, {
      ...this.HttpOnlyCookieOpts,
      expires: dayjs().add(REFRESH_TOKEN_EXPIRES_IN, 's').toDate()
    });
  }

  private saveUserDataCookie(response: Response, userId: number) {
    response.cookie('u', JSON.stringify({ id: userId }), this.userCookieOpts);
  }

  private generateToken(userId: number): {
    accessToken: string;
    expiresIn: number;
    refreshToken: string;
  } {
    try {
      const signKey = { key: privateKey, passphrase: String(PRIVATE_KEY_PASSPHRASE) };
      const refreshKey = {
        key: refreshPrivateKey,
        passphrase: String(REFRESH_PRIVATE_KEY_PASSPHRASE)
      };
      const opts: jwt.SignOptions = { expiresIn: TOKEN_EXPIRES_IN };
      const refreshOpts: jwt.SignOptions = { expiresIn: REFRESH_TOKEN_EXPIRES_IN };

      opts.algorithm = 'RS256';
      refreshOpts.algorithm = 'RS256';

      const accessToken = jwt.sign(
        {
          id: userId
        },
        signKey,
        opts
      );

      const refreshToken = jwt.sign(
        {
          id: userId
        },
        refreshKey,
        refreshOpts
      );

      return {
        refreshToken,
        accessToken,
        expiresIn: TOKEN_EXPIRES_IN
      };
    } catch (e) {
      loggerHelper.error(e);
      throw e;
    }
  }

  async validateRefreshToken(token: string): Promise<{ id: number }> {
    try {
      const opts: VerifyOptions = {};
      opts.algorithms = ['RS256'];
      const decoded = jwt.verify(token, refreshPublicKey, opts) as {
        id: number;
        iat: number;
        exp: number;
      };
      return typeof decoded === 'string' ? { id: 0 } : { id: decoded.id };
    } catch (err) {
      loggerHelper.error(err);
      throw err;
    }
  }

  public async validateAccessToken(token: string): Promise<{ id: number }> {
    try {
      const opts: VerifyOptions = {};
      opts.algorithms = ['RS256'];
      const decoded = jwt.verify(token, publicKey, opts) as {
        id: number;
        iat: number;
        exp: number;
      };
      return typeof decoded === 'string' ? { id: 0 } : { id: decoded.id };
    } catch (err) {
      loggerHelper.error(err);
      throw err;
    }
  }

  public async deleteToken(response: Response, request: Request) {
    const accessToken = request.signedCookies[REFRESH_TOKEN_COOKIE_NAME];
    if (!accessToken) return;
    this.deleteCookie(response, REFRESH_TOKEN_COOKIE_NAME);
    this.deleteCookie(response, ACCESS_TOKEN_COOKIE_NAME);
    this.deleteCookie(response, 'u');
    try {
      const exist = await this.tokenRepo.findOneOrFail({ where: { accessToken } });
      await this.tokenRepo.remove(exist);
    } catch {
      loggerHelper.error('wrong token -', accessToken);
    }
  }

  public async createTokens(response: Response, request: Request, userId: number) {
    const data = this.generateToken(userId);

    const {
      //@ts-ignore
      fingerprint: { hash, components }
    } = request;

    const exist = await this.tokenRepo.findOne({ where: { hash } });
    if (exist) await this.tokenRepo.remove(exist);

    await this.tokenRepo.save({
      accessToken: data.accessToken,
      refreshToken: data.refreshToken,
      userId,
      hash,
      deviceData: components
    });

    this.saveAccessTokenCookie(response, data.accessToken);
    this.saveRefreshTokenCookie(response, data.refreshToken);
    this.saveUserDataCookie(response, userId);

    return data;
  }

  public async sendAuthCode(phone: string, code: number) {
    const modifiedPhone = `998${phone}`;
    try {
      await sendSMS({ to: modifiedPhone, body: `Rent ${code} is your verification code.` });
    } catch (e) {
      loggerHelper.error(e);
      throw new Error("SMS wasn't sent!");
    }
  }
}
