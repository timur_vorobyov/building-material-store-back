import express from 'express';
import AuthController from './auth.controller';
const router = express.Router();

const controller = new AuthController();
router.post('/login', controller.login.bind(controller));
router.post('/log-out', controller.logOut.bind(controller));
router.post('/sign-up', controller.signUp.bind(controller));
router.post('/send-verify-code-sign-up', controller.sendVerifyCodeSignUp.bind(controller));
router.post('/send-verify-code-login', controller.sendVerifyCodeLogin.bind(controller));

export default router;
