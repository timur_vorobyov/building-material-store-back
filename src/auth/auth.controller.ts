import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { UserService } from '../user/user.service';
import { SuccessResponse } from '../utils/response/success.response';
import AuthService from './auth.service';
import { CreateUserDto } from '../user/dto/create-user.dto';
import argon2 from 'argon2';

export default class AuthController {
  private authService;
  private userService;

  constructor() {
    this.authService = new AuthService();
    this.userService = new UserService();
  }

  async sendVerifyCodeLogin(
    request: Request<Record<string, never>, Record<string, never>, { phone: string }>,
    response: Response,
    next: NextFunction
  ) {
    try {
      const { phone } = request.body;
      const user = await this.userService.findByPhone(phone);

      if (!user) {
        return response.send(SuccessResponse({ data: `PHONE_NUMBER_DOES_NOT_EXIST` }));
      }

      const code = Math.floor(Math.random() * 999999) + 100000;
      await this.userService.update(user.id, { password: await argon2.hash(String(code)) });
      await this.authService.sendAuthCode(phone, code);
      return response.send(SuccessResponse({ data: `Verification code sent to ${phone}` }));
    } catch (e) {
      next(e);
    }
  }

  async login(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      { phone: string; password: string }
    >,
    response: Response
  ) {
    const { phone, password } = request.body;
    const user = await this.userService.findByPhone(phone);

    if (!user || !(await user!.comparePassword(password))) {
      return response.send(SuccessResponse({ data: {} }));
    }

    const tokenData = await this.authService.createTokens(response, request, user.id);

    return response.send(SuccessResponse({ data: tokenData }));
  }

  async sendVerifyCodeSignUp(
    request: Request<Record<string, never>, Record<string, never>, { phone: string }>,
    response: Response
  ) {
    const { phone } = request.body;
    const user = await this.userService.findByPhone(phone);

    if (user) {
      return response.send(SuccessResponse({ data: `PHONE_NUMBER_ALREADY_EXIST` }));
    }

    const code = Math.floor(Math.random() * 999999) + 100000;
    request.app.locals.code = code;
    await this.authService.sendAuthCode(phone, code);
    return response.send(SuccessResponse({ data: `Verification code sent to ${phone}` }));
  }

  async signUp(
    request: Request<Record<string, never>, Record<string, never>, Omit<CreateUserDto, 'isAdmin'>>,
    response: Response,
    next: NextFunction
  ) {
    const { name, phone, password } = request.body;
    const code = request.app.locals.code;

    if (password !== code.toString()) {
      return response.send(SuccessResponse({ data: {} }));
    }

    try {
      const userData = await this.userService.create({
        name,
        phone,
        isAdmin: false,
        password: await argon2.hash(password)
      });

      const tokenData = await this.authService.createTokens(response, request, userData.id);
      return response.send(SuccessResponse({ data: { ...tokenData, userData } }));
    } catch (e) {
      next(e);
    }
  }

  async logOut(
    request: Request<Record<string, never>, Record<string, never>, Omit<CreateUserDto, 'isAdmin'>>,
    response: Response,
    next: NextFunction
  ) {
    try {
      await this.authService.deleteToken(response, request);
      return response.json({ data: {} });
    } catch (e) {
      next(e);
    }
  }
}
