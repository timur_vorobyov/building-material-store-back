import { SuccessResponse } from '@utils/response/success.response';
import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { CreateProductCartDto } from '../dto/create-product-cart.dto';
import { QueryProductCartDto } from '../dto/query-product-cart.dto';
import { RemoveProductCartDto } from '../dto/remove-product-cart.dto';
import { ProductCartService } from '../services/product-cart.service';

export default class ProductCartController {
  private productCartService;

  constructor() {
    this.productCartService = new ProductCartService();
  }

  async add(
    request: Request<Record<string, never>, Record<string, never>, CreateProductCartDto>,
    response: Response,
    next: NextFunction
  ) {
    try {
      const cart = await this.productCartService.create(request.body);

      return response.send(
        SuccessResponse({
          data: { ...cart },
          message: 'PRODCUT_IS_ADDED_TO_CART'
        })
      );
    } catch (e) {
      next(e);
    }
  }

  async remove(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      RemoveProductCartDto
    >,
    response: Response,
    next: NextFunction
  ) {
    const { userId, productId } = request.query;

    try {
      const cart = await this.productCartService.remove({ userId: +userId, productId: +productId });
      return response.send(
        SuccessResponse({
          data: { ...cart },
          message: 'PRODCUT_IS_REMOVED_FROM_CART'
        })
      );
    } catch (e) {
      next(e);
    }
  }

  async findAll(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      QueryProductCartDto
    >,
    response: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.productCartService.findAll(request.query);

      return response.send(SuccessResponse({ data }));
    } catch (e) {
      next(e);
    }
  }
}
