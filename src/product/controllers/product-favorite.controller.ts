import { SuccessResponse } from '@utils/response/success.response';
import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { CreateProductFavoriteDto } from '../dto/create-product-favorite.dto';
import { QueryFavoriteProductDto } from '../dto/query-product-favorite.dto';
import { RemoveProductFavoriteDto } from '../dto/remove-product-favorite.dto';
import { ProductFavoriteService } from '../services/product-favorite.service';

export default class ProductFavoriteController {
  private productFavoriteService;

  constructor() {
    this.productFavoriteService = new ProductFavoriteService();
  }

  async create(
    request: Request<Record<string, never>, Record<string, never>, CreateProductFavoriteDto>,
    response: Response,
    next: NextFunction
  ) {
    try {
      const product = await this.productFavoriteService.create(request.body);

      return response.send(
        SuccessResponse({ data: product, message: 'PRODUCT_IS_ADDED_TO_FAVORITE' })
      );
    } catch (e) {
      next(e);
    }
  }

  async remove(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      RemoveProductFavoriteDto
    >,
    response: Response,
    next: NextFunction
  ) {
    const { userId, productId } = request.query;
    try {
      const product = await this.productFavoriteService.remove(+userId, +productId);

      return response.send(
        SuccessResponse({ data: product, message: 'PRODUCT_IS_REMOVED_FROM_FAVORITE' })
      );
    } catch (e) {
      next(e);
    }
  }

  async findAll(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      QueryFavoriteProductDto
    >,
    response: Response,
    next: NextFunction
  ) {
    try {
      const products = await this.productFavoriteService.findAll(request.query);
      return response.send(SuccessResponse({ data: products }));
    } catch (e) {
      next(e);
    }
  }
}
