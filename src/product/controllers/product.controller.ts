import { SuccessResponse } from '@utils/response/success.response';
import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { CreateProductDto } from '../dto/create-product.dto';
import { QueryProductDto } from '../dto/query-product.dto';
import { ProductService } from '../services/product.service';

export default class ProductController {
  private productService;

  constructor() {
    this.productService = new ProductService();
  }

  async create(
    request: Request<Record<string, never>, Record<string, never>, CreateProductDto>,
    response: Response,
    next: NextFunction
  ) {
    try {
      await this.productService.create(request.body);

      return response.send(SuccessResponse({ data: 'AD_IS_CREATED' }));
    } catch (e) {
      next(e);
    }
  }

  async findAll(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      QueryProductDto
    >,
    response: Response,
    next: NextFunction
  ) {
    try {
      const [data, total] = await this.productService.findAll(request.query);

      return response.send(SuccessResponse({ data, total }));
    } catch (e) {
      next(e);
    }
  }

  async findOneById(
    request: Request<{ id: number }, Record<string, never>, Record<string, never>>,
    response: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.productService.findOneById(request.params.id);
      return response.send(SuccessResponse({ data }));
    } catch (e) {
      next(e);
    }
  }

  async findAllByUserId(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      { userId: number }
    >,
    response: Response,
    next: NextFunction
  ) {
    const { userId } = request.query;
    try {
      const data = await this.productService.findAllByUserId(Number(userId));
      return response.send(SuccessResponse({ data: data?.products }));
    } catch (e) {
      next(e);
    }
  }
}
