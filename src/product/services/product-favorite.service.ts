import UserEntity from '@root/src/user/entities/user.entity';
import MainDataSource from '@root/src/utils/main-data-source';
import { CreateProductFavoriteDto } from '../dto/create-product-favorite.dto';
import { QueryFavoriteProductDto } from '../dto/query-product-favorite.dto';
import ProductEntity from '../entities/product.entity';

export class ProductFavoriteService {
  private product;
  private user;

  constructor() {
    this.user = MainDataSource.getRepository(UserEntity);
    this.product = MainDataSource.getRepository(ProductEntity);
  }

  public async create(createproductDto: CreateProductFavoriteDto) {
    const product = await this.product.findOne({ where: { id: createproductDto.productId } });
    const user = await this.user.findOne({
      where: { id: createproductDto.userId },
      relations: {
        favorite: true
      }
    });
    if (user && product) {
      user.favorite.push(product);
      await this.user.save(user);
    }

    return product;
  }

  public async remove(userId: number, productId: number) {
    const product = await this.product.findOne({ where: { id: productId } });
    const user = await this.user.findOne({
      where: { id: userId },
      relations: {
        favorite: true
      }
    });
    if (user && product) {
      user.favorite = user.favorite.filter((product) => {
        return product.id !== productId;
      });
      await this.user.save(user);
    }
    return product;
  }

  public async findAll(params: QueryFavoriteProductDto) {
    const user = await this.user.findOne({
      where: { id: params.userId },
      relations: {
        favorite: true
      }
    });

    return user?.favorite;
  }
}
