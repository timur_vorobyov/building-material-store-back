import UserEntity from '@root/src/user/entities/user.entity';
import MainDataSource from '@root/src/utils/main-data-source';
import { CreateProductCartDto } from '../dto/create-product-cart.dto';
import { QueryProductCartDto } from '../dto/query-product-cart.dto';
import { RemoveProductCartDto } from '../dto/remove-product-cart.dto';
import { ProductCartEntity } from '../entities/product-cart.entity';
import ProductEntity from '../entities/product.entity';

export class ProductCartService {
  private product;
  private user;
  private cart;

  constructor() {
    this.user = MainDataSource.getRepository(UserEntity);
    this.product = MainDataSource.getRepository(ProductEntity);
    this.cart = MainDataSource.getRepository(ProductCartEntity);
  }

  public async create(createProductDto: CreateProductCartDto): Promise<ProductCartEntity> {
    const { productId, userId } = createProductDto;
    const cart = await this.cart.findOne({
      where: {
        product: { id: productId },
        user: { id: userId }
      },
      relations: {
        product: true
      }
    });

    if (cart) {
      cart.count++;
      await this.cart.save(cart);

      return cart;
    }
    const user = await this.user.findOneOrFail({
      where: { id: userId }
    });
    const product = await this.product.findOneOrFail({
      where: { id: productId }
    });

    if (user && product) {
      const newCart = this.cart.create({ user, product, count: 1 });
      return await this.cart.save(newCart);
    }

    throw new Error('USER_OR_PRODUCT_IS_NOT_FOUND');
  }

  public async remove(removeProductDto: RemoveProductCartDto): Promise<ProductCartEntity> {
    const { productId, userId } = removeProductDto;
    const cart = await this.cart.findOne({
      where: {
        product: { id: productId },
        user: { id: userId }
      },
      relations: {
        product: true
      }
    });

    if (cart) {
      const count = cart.count - 1;
      if (count === 0) {
        await this.cart.delete(cart.id);
        cart.count--;
        return cart;
      }

      cart.count--;
      return await this.cart.save(cart);
    }

    throw new Error('USER_OR_PRODUCT_IS_NOT_FOUND');
  }

  public async findAll(params: QueryProductCartDto) {
    const { userId } = params;
    const cart = await this.cart.find({
      where: {
        user: { id: userId }
      },
      relations: {
        product: true
      }
    });

    return cart;
  }
}
