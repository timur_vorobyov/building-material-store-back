import UserEntity from '@root/src/user/entities/user.entity';
import loggerHelper from '@root/src/utils/helpers/logger.helper';
import { prepareImage, uploadToS3 } from '@root/src/utils/helpers/s3';
import MainDataSource from '@root/src/utils/main-data-source';
import { CreateProductDto } from '../dto/create-product.dto';
import { QueryProductDto } from '../dto/query-product.dto';
import ProductEntity from '../entities/product.entity';

export class ProductService {
  private repo;
  private userRepo;

  constructor() {
    this.repo = MainDataSource.getRepository(ProductEntity);
    this.userRepo = MainDataSource.getRepository(UserEntity);
  }

  public async create(createProductDto: CreateProductDto) {
    const { imageUrls, userId, id, ...cloneData } = createProductDto;
    let savedProduct: ProductEntity;

    const user = await MainDataSource.getRepository(UserEntity).findOneOrFail({
      where: { id: userId }
    });

    if (id) {
      const entity = await this.findOneById(id);
      savedProduct = await this.repo.save({ ...entity, ...cloneData, user });
    } else {
      savedProduct = await this.repo.create({ ...cloneData, user });
    }

    if (imageUrls && imageUrls.length) {
      savedProduct.imagesUrls = [];
      await Promise.all(
        imageUrls.map(async (imageUrl) => {
          try {
            const image64 = await prepareImage(imageUrl, 1024);
            const file = await uploadToS3(
              image64,
              'ad',
              String(savedProduct.id),
              String(new Date().valueOf())
            );
            savedProduct.imagesUrls.push(file);
          } catch (e) {
            loggerHelper.error('image saving failed - ', e);
          }
        })
      );
    }

    return this.repo.save(savedProduct);
  }

  public findOneById(id: number) {
    return this.repo.findOne({
      where: { id },
      relations: {
        category: true,
        brand: true
      }
    });
  }

  public async findAllByUserId(id: number) {
    return this.userRepo.findOne({
      where: { id },
      relations: {
        products: true
      }
    });
  }

  public async findAll(params: QueryProductDto) {
    const { take = 10, skip = 0 } = params;
    const query = this.repo.createQueryBuilder('product');
    // if (price) {
    //   query.where({
    //     price: Between(price[0], price[1])
    //   });
    // }

    return query.take(take).skip(skip).getManyAndCount();
  }
}
