export interface RemoveProductCartDto {
  userId: number;
  productId: number;
}
