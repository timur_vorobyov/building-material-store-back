export interface CreateProductDto {
  id?: number;
  userId: number;
  price?: number;
  title?: string;
  desc?: string;
  imageUrls?: string[];
}
