export interface CreateProductCartDto {
  userId: number;
  productId: number;
  count?: number;
}
