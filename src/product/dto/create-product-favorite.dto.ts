export interface CreateProductFavoriteDto {
  userId: number;
  productId: number;
}
