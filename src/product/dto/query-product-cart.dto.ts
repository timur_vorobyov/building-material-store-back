import { BaseQueryDto } from '@root/src/utils/dto/base-query.dto';

export interface QueryProductCartDto extends BaseQueryDto {
  userId: number;
}
