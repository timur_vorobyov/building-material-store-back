export interface RemoveProductFavoriteDto {
  userId: number;
  productId: number;
}
