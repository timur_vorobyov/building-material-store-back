import { BaseQueryDto } from '@root/src/utils/dto/base-query.dto';

export interface QueryProductDto extends BaseQueryDto {
  price: number[];
  userId: number;
}
