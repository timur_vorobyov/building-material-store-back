import express from 'express';
import ProductController from '../controllers/product.controller';
const router = express.Router();

const controller = new ProductController();
router.get('/owner-list', controller.findAllByUserId.bind(controller));
router.post('/', controller.create.bind(controller));
router.get('/:id', controller.findOneById.bind(controller));
router.get('/', controller.findAll.bind(controller));
//router.delete('/:id', controller.remove.bind(controller));

export default router;
