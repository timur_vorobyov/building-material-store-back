import express from 'express';
import ProductFavoriteController from '../controllers/product-favorite.controller';
const router = express.Router();

const controller = new ProductFavoriteController();
router.post('/', controller.create.bind(controller));
// router.put('/:id', controller.update.bind(controller));
// router.get('/:id', controller.findOneById.bind(controller));
router.get('/', controller.findAll.bind(controller));
router.delete('/', controller.remove.bind(controller));

export default router;
