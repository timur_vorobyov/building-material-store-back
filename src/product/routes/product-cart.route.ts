import express from 'express';
import ProductCartController from '../controllers/product-cart.controller';
const router = express.Router();

const controller = new ProductCartController();
router.post('/', controller.add.bind(controller));
// router.put('/:id', controller.update.bind(controller));
// router.get('/:id', controller.findOneById.bind(controller));
router.get('/', controller.findAll.bind(controller));
router.delete('/', controller.remove.bind(controller));

export default router;
