import { Column, Entity, OneToMany } from 'typeorm';
import BaseEntity from '@root/src/utils/base.entity';
import ProductEntity from './product.entity';

@Entity({ name: 'category', schema: 'order' })
export default class ProductCategoryEntity extends BaseEntity {
  @Column()
  nameRu: string;

  @Column()
  nameEn: string;

  @Column()
  nameUz: string;

  @Column()
  imageUrl: string;

  @OneToMany(() => ProductEntity, (productEntity) => productEntity.category)
  product: ProductEntity[];
}
