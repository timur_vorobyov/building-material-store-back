import BaseEntity from '@root/src/utils/base.entity';
import { Entity, Column, PrimaryGeneratedColumn, OneToMany } from 'typeorm';
import ProductEntity from './product.entity';

@Entity({ name: 'brand', schema: 'order' })
export class ProductBrandEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @OneToMany(() => ProductEntity, (productEntity) => productEntity.category)
  product: ProductEntity[];
}
