import { Column, Entity, JoinTable, ManyToMany, ManyToOne, OneToMany } from 'typeorm';
import BaseEntity from '@root/src/utils/base.entity';
import ProductCategoryEntity from './product-category.entity';
import UserEntity from '@root/src/user/entities/user.entity';
import { ProductCartEntity } from './product-cart.entity';
import { ProductBrandEntity } from './product-brand.entity';

@Entity({ name: 'product', schema: 'order' })
export default class ProductEntity extends BaseEntity {
  @Column({ default: 0 })
  price: number;

  @Column()
  nameRu: string;

  @Column()
  nameEn: string;

  @Column()
  nameUz: string;

  @Column()
  descRu: string;

  @Column()
  descEn: string;

  @Column()
  descUz: string;

  @Column('simple-array', { default: [], nullable: false })
  imagesUrls: string[];

  @ManyToOne(() => ProductCategoryEntity, (ProductCategory) => ProductCategory.product)
  category: ProductCategoryEntity;

  @ManyToOne(() => ProductBrandEntity, (ProductBrandEntity) => ProductBrandEntity.product)
  brand: ProductBrandEntity;

  @ManyToOne(() => UserEntity, (user) => user.products, {
    nullable: false
  })
  user: UserEntity;

  @ManyToMany(() => UserEntity, (u) => u.favorite, { onDelete: 'CASCADE' })
  @JoinTable({
    schema: 'order',
    name: 'favorite',
    joinColumn: {
      name: 'productId',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'userId',
      referencedColumnName: 'id'
    }
  })
  favorite?: UserEntity[];

  @OneToMany(() => ProductCartEntity, (cart) => cart.user)
  cart: ProductCartEntity[];
}
