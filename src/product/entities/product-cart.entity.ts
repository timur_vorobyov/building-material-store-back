import UserEntity from '@root/src/user/entities/user.entity';
import BaseEntity from '@root/src/utils/base.entity';
import { Entity, Column, PrimaryGeneratedColumn, ManyToOne } from 'typeorm';
import ProductEntity from './product.entity';

@Entity()
export class ProductCartEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ default: 0 })
  count: number;

  @ManyToOne(() => UserEntity, (user) => user.cart)
  user: UserEntity;

  @ManyToOne(() => ProductEntity, (product) => product.cart)
  product: ProductEntity;
}
