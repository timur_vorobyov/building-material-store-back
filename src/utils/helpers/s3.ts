import AWS from 'aws-sdk';
import { DO_ACCESS_KEY, DO_BUCKET_NAME, DO_SECRET_KEY, DO_SPACES_URL } from '../config';
import loggerHelper from './logger.helper';
import Jimp from 'jimp';

const s3Client: AWS.S3 = new AWS.S3({
  endpoint: DO_SPACES_URL,
  credentials: {
    accessKeyId: DO_ACCESS_KEY,
    secretAccessKey: DO_SECRET_KEY
  }
});

const prepareImage = async (imageData64: string, maxWidth: number): Promise<string> => {
  const url = imageData64.replace(/^data:image\/\w+;base64,/, '');
  // @ts-ignore
  // eslint-disable-next-line new-cap
  const buffer = new Buffer.from(url, 'base64');

  const jimpImage = await Jimp.read(buffer);

  if (jimpImage.bitmap.width > maxWidth) {
    return jimpImage.resize(maxWidth, Jimp.AUTO).quality(80).getBase64Async(jimpImage.getMIME());
  }
  return imageData64;
};

const uploadToS3 = async (base64: string, entityType: string, itemId: string, filename: string) => {
  // @ts-ignore
  // eslint-disable-next-line new-cap
  const base64Data = new Buffer.from(base64.replace(/^data:.+;base64,/, ''), 'base64');

  const type = base64.split(';')[0].split('/')[1];

  const fileUrl = `static/${entityType}/${itemId}/${filename}.${type}`;

  const params: any = {
    Bucket: DO_BUCKET_NAME,
    Key: fileUrl,
    Body: base64Data,
    ACL: 'public-read',
    ContentEncoding: 'base64',
    ContentType: `image/${type}`
  };

  try {
    await s3Client.upload(params).promise();
    return `/${fileUrl}`;
  } catch (e) {
    loggerHelper.error('Save to S3 error', e);
    throw e;
  }
};
export { uploadToS3, prepareImage };
