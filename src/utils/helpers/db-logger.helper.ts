/* eslint-disable */
import { Logger } from 'typeorm';
import { dbConsoleLoggerHelper, dbFileLoggerHelper } from '@helpers/logger.helper';
import clc from 'cli-color';

export class DbLoggerHelper implements Logger {
  logQuery(query: string, parameters?: any[] | undefined) {
    // const message = `${query} -- Parameters: ${this.stringifyParameters(parameters)}`;
    // dbConsoleLoggerHelper.log({ level: 'error', message });
  }

  logQueryError(error: string | Error, query: string, parameters?: any[]): any {
    let message = ` ${clc.red.bold(typeof error === 'string' ? error : error.message)}`;
    message += `\n${clc.red.bold('QUERY')}: ${clc.yellow(query)}`;

    if (parameters) {
      message += `\n${clc.red.bold('PARAMETERS')}: ${clc.red('{')}`;
      parameters.forEach(
        (p: string, i) =>
          (message += clc.red(`\n                  $${i + 1}: ${clc.yellowBright.bold(p)},`))
      );
      message += `\n            ${clc.red('}')}`;
    }

    dbFileLoggerHelper.log({
      level: 'error',
      message: typeof error === 'string' ? error : error.message,
      query,
      parameters
    });
    dbConsoleLoggerHelper.log({ level: 'error', message });
    return;
  }

  logQuerySlow(time: number, query: string, parameters?: any[]): any {
    let message = ` ${clc.red.bold(`SLOW QUERY TOOK ${time}`)}`;
    message += `\n${clc.red.bold('QUERY')}: ${clc.cyan(query)}`;

    if (parameters) {
      message += `\n${clc.red.bold('PARAMETERS')}: ${clc.cyan('{')}`;
      parameters.forEach(
        (p: string, i) =>
          (message += clc.cyan(`\n                  $${i + 1}: ${clc.yellowBright.bold(p)},`))
      );
      message += `\n            ${clc.cyan('}')}`;
    }

    dbFileLoggerHelper.log({
      level: 'error',
      message: `SLOW QUERY TOOK ${time}`,
      query,
      parameters
    });
    dbConsoleLoggerHelper.error(message);
    return;
  }

  logSchemaBuild(message: string): any {
    dbConsoleLoggerHelper.log({ level: 'info', message });
  }

  logMigration(message: string) {
    dbConsoleLoggerHelper.log({ level: 'error', message });
  }

  log(level: 'warn' | 'info' | 'log', message: any) {
    if (level === 'log') {
      dbConsoleLoggerHelper.log({ level: 'log', message });
    }
    if (level === 'info') {
      dbConsoleLoggerHelper.log({ level: 'info', message });
    }
    if (level === 'warn') {
      dbConsoleLoggerHelper.log({ level: 'warn', message });
    }
  }

  // private stringifyParameters(parameters?: unknown[]) {
  //   try {
  //     return JSON.stringify(parameters);
  //   } catch {
  //     return '';
  //   }
  // }
}
