import axios from 'axios';
import FormData from 'form-data';
import {
  ESKIZ_SMS_SERVICE_AUTH_URL,
  ESKIZ_SMS_SERVICE_EMAIL,
  ESKIZ_SMS_SERVICE_PASSWORD
} from '../config';

interface SMS {
  body: string;
  to: string;
}

export const sendSMS = async (sms: SMS) => {
  const { to, body } = sms;

  const authData = new FormData();

  authData.append('email', ESKIZ_SMS_SERVICE_EMAIL);
  authData.append('password', ESKIZ_SMS_SERVICE_PASSWORD);

  const authConfig = {
    method: 'post',
    url: ESKIZ_SMS_SERVICE_AUTH_URL,
    headers: {
      ...authData.getHeaders()
    },
    data: authData
  };

  const authResponse = await axios(authConfig);

  const data = new FormData();

  data.append('mobile_phone', to);
  data.append('message', body);
  data.append('from', '4546');
  const config = {
    method: 'post',
    url: 'https://notify.eskiz.uz/api/message/sms/send',
    headers: {
      ...data.getHeaders(),
      Authorization: `Bearer ${authResponse.data.data.token}`
    },
    data: data
  };

  await axios(config);
};
