import { createLogger, transports, format } from 'winston';
import dayjs from 'dayjs';
import clc from 'cli-color';
import 'winston-daily-rotate-file';
import { LOG_DATE_FORMAT, LOG_PATH, NODE_ENV } from '@utils/config';

const { combine, timestamp, prettyPrint, errors, printf } = format;

const loggerHelper = createLogger({
  exitOnError: false,
  format: combine(errors({ stack: true }), timestamp(), prettyPrint(), format.json()),
  transports: [
    new transports.DailyRotateFile({
      json: true,
      filename: `${LOG_PATH}/api/error-%DATE%.log`,
      datePattern: LOG_DATE_FORMAT,
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      level: 'error'
    })
  ]
});

const dbConsoleLoggerHelper = createLogger({
  exitOnError: false,
  format: combine(errors({ stack: true }), timestamp(), prettyPrint(), format.json()),
  transports: [
    new transports.Console({
      format: combine(
        format.colorize(),
        timestamp(),
        printf((info) => {
          const {
            level,
            message,
            timestamp: printTimestamp,
            name,
            query,
            parameters,
            stack
          } = info;
          return `${clc.blue.bold(dayjs(printTimestamp).format('D MMMM YYYY HH:mm:ss'))} ${clc.bold(
            `DB LOG`
          )} ${name ? `[${name}] ` : ''}${level}: ${message} ${query ? `\n QUERY: ${query}` : ''} ${
            parameters ? `\n PARAMS: ${parameters}` : ''
          } ${stack ? `\n ${stack}` : ''}`;
        })
      )
    })
  ]
});

const dbFileLoggerHelper = createLogger({
  exitOnError: false,
  format: combine(errors({ stack: true }), timestamp(), prettyPrint(), format.json()),
  transports: [
    new transports.DailyRotateFile({
      json: true,
      filename: `${LOG_PATH}/db/error-%DATE%.log`,
      datePattern: LOG_DATE_FORMAT,
      zippedArchive: true,
      maxSize: '20m',
      maxFiles: '14d',
      level: 'error'
    })
  ]
});

if (NODE_ENV !== 'production') {
  const consoleFormat = (type: string) =>
    printf((info) => {
      const { level, message, timestamp: printTimestamp, name, query, parameters, stack } = info;
      return `${clc.blue.bold(dayjs(printTimestamp).format('D MMMM YYYY HH:mm:ss'))} ${clc.bold(
        `${type} LOG`
      )} ${name ? `[${name}] ` : ''}${level}: ${message} ${query ? `\n QUERY: ${query}` : ''} ${
        parameters ? `\n PARAMS: ${parameters}` : ''
      } ${stack ? `\n ${stack}` : ''}`;
    });

  loggerHelper.add(
    new transports.Console({
      format: combine(format.colorize(), timestamp(), consoleFormat('COMMON'))
    })
  );
}

export { dbConsoleLoggerHelper, dbFileLoggerHelper };

export default loggerHelper;
