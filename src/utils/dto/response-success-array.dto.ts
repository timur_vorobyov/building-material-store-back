/* eslint-disable @typescript-eslint/no-explicit-any*/
export class ResponseSuccessArrayDto {
  status: string;

  data: any[];

  total: number;

  constructor(data: any[], total: number) {
    this.status = 'success';
    this.data = data;
    this.total = total;
  }
}
