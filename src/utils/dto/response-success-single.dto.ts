/* eslint-disable @typescript-eslint/no-explicit-any*/
export class ResponseSuccessSingleDto {
  status: 'success';

  data: any;

  constructor(data: any) {
    this.status = 'success';
    this.data = data;
  }
}
