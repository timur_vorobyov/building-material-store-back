import { CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from 'typeorm';
import BaseInterface from './base.interface';

@Entity()
export default abstract class BaseEntity implements BaseInterface {
  @PrimaryGeneratedColumn('increment')
  id: number;

  /**
   * Created at timestamp
   */
  @CreateDateColumn({
    name: 'createdAt',
    default: () => 'NOW()',
    type: 'timestamptz'
  })
  readonly createdAt: Date;

  /**
   * Updated at timestamp
   */
  @UpdateDateColumn({
    name: 'updatedAt',
    default: () => 'NOW()',
    type: 'timestamptz'
  })
  readonly updatedAt: Date;
}
