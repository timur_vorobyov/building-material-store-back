import { DataSource } from 'typeorm';
import dotenv from 'dotenv';
import path from 'path';
import { DataSourceOptions } from 'typeorm/data-source/DataSourceOptions';
import { NODE_ENV } from './config';
import { DbLoggerHelper } from './helpers/db-logger.helper';

if (process.env.NODE_ENV === 'test') {
  dotenv.config({ path: path.join(__dirname, '../', '.env.test') });
} else {
  dotenv.config();
}

const {
  POSTGRES_HOST: ENV_POSTGRES_HOST,
  POSTGRES_PORT: ENV_POSTGRES_PORT,
  POSTGRES_DB: ENV_POSTGRES_DB,
  POSTGRES_PASSWORD: ENV_POSTGRES_PASSWORD,
  POSTGRES_USER: ENV_POSTGRES_USER
} = process.env;

const POSTGRES_HOST = ENV_POSTGRES_HOST || 'localhost';
const POSTGRES_PORT = ENV_POSTGRES_PORT || 5432;
const POSTGRES_DB = ENV_POSTGRES_DB || 'testing_db';
const POSTGRES_PASSWORD = ENV_POSTGRES_PASSWORD || 'any_test_pass';
const POSTGRES_USER = ENV_POSTGRES_USER || 'testing_user';

const ORM_CONFIG: DataSourceOptions = {
  type: 'postgres',
  host: POSTGRES_HOST,
  port: Number(POSTGRES_PORT),
  database: POSTGRES_DB,
  password: POSTGRES_PASSWORD,
  username: POSTGRES_USER,
  migrationsRun: false,
  entities: ['./**/*.entity.ts'],
  subscribers: [],
  migrations: ['src/utils/migrations/dev.migration.ts'],
  logger: NODE_ENV === 'test' ? undefined : new DbLoggerHelper(),
  logging: NODE_ENV !== 'test',
  // https://node-postgres.com/apis/pool
  extra:
    NODE_ENV === 'test'
      ? {}
      : {
          max: 10,
          idleTimeoutMillis: 10000,
          allowExitOnIdle: true,
          connectionTimeoutMillis: 20000
        },
  cache: false
};

const MainDataSource = new DataSource(ORM_CONFIG);
export default MainDataSource;
