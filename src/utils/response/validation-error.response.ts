/* eslint-disable @typescript-eslint/no-explicit-any*/
export interface ValidatorErrorResponseInterface {
  data?: any[];
  message?: string | undefined;
}

export class ValidatorErrorResponse implements ValidatorErrorResponseInterface {
  code: number;
  message?: string | undefined;
  status = 'error';
  data?: any[];

  constructor(props?: ValidatorErrorResponseInterface) {
    this.code = 422;
    this.message = props?.message || 'Validation error!';
    this.data = props?.data || [];
  }
}
