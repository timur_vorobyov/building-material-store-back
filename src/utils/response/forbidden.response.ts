import { ErrorResponse, ErrorResponseInterface } from './error.response';

export class ForbiddenResponse extends ErrorResponse {
  code = 403;
  message: string | undefined;

  constructor(props?: ErrorResponseInterface) {
    super(props);
    this.message = props?.message || 'Insufficient permissions';
  }
}
