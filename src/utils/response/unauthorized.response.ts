import { ErrorResponse, ErrorResponseInterface } from './error.response';

export class UnauthorizedResponse extends ErrorResponse {
  code = 401;
  message: string | undefined;

  constructor(props?: ErrorResponseInterface) {
    super(props);
    this.message = props?.message || 'Unauthorized';
  }
}
