/* eslint-disable @typescript-eslint/no-explicit-any*/
export interface ErrorResponseInterface {
  status?: string;
  code?: number | undefined;
  message?: string | undefined;
  total?: number | undefined;
  // @ts-ignore
  data?: any;
}

export class ErrorResponse {
  code: number;
  message: string | undefined = 'Internal server error';
  status = 'error';
  total?: number | undefined;
  // @ts-ignore
  data?: any;

  constructor(props?: ErrorResponseInterface) {
    this.status = 'error';
    this.code = props?.code || 500;
    this.message = props?.message;
    this.data = props?.data;
    this.total = Array.isArray(props?.data) ? props?.data.length : props?.total;
  }
}
