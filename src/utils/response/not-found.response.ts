import { ErrorResponse, ErrorResponseInterface } from './error.response';

export class NotFoundErrorResponse extends ErrorResponse {
  code = 404;
  message: string | undefined;

  constructor(props?: ErrorResponseInterface) {
    super(props);
    this.message = props?.message || 'Not found';
  }
}
