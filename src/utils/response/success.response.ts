export interface SuccessResponseInterface<T> {
  status?: string;
  code?: number | undefined;
  total?: number | undefined;
  message?: string | undefined;
  data: T;
}

export const SuccessResponse = <T>({ data, code, message, total }: SuccessResponseInterface<T>) => {
  return {
    status: 'success',
    code: code || 200,
    message: message,
    data: data,
    total: total
  };
};
