import 'dotenv/config';
import express from 'express';
import cookieParser from 'cookie-parser';
import helmet from 'helmet';
import 'reflect-metadata';
import MainDataSource from '@utils/main-data-source';
import { CORS_ORIGIN, PORT } from '@utils/config';
import cors from 'cors';
import authRoute from './auth/auth.routes';
import errorHandler from './middleware/error.middleware/error.middleware';
import loggerHelper from './utils/helpers/logger.helper';
import userRoute from './user/user.route';
import productRoute from './product/routes/product.route';
import productFavoriteRoute from './product/routes/product-favorite.route';
import productCartRoute from './product/routes/product-cart.route';
import orderRoute from './order/routes/order.route';

//@ts-ignore
const Fingerprint = require('express-fingerprint');

const app = express();
app.use(express.json({ limit: '50mb' }));
app.use(express.static('public'));
app.use(
  cors({
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE,OPTIONS',
    origin: CORS_ORIGIN,
    credentials: true,
    optionsSuccessStatus: 200,
    preflightContinue: false,
    allowedHeaders: [
      'Origin',
      'X-Requested-With',
      'Content-Type',
      'Accept',
      'auth',
      'Cookies',
      'set-cookie'
    ],
    exposedHeaders: ['Content-Type', 'token', 'auth', 'Cookies', 'set-cookie']
  })
);
app.use(
  helmet({
    crossOriginResourcePolicy: false
  })
);
app.use(
  Fingerprint({
    parameters: [Fingerprint.useragent, Fingerprint.geoip]
  })
);
app.use(cookieParser());
app.use('/auth', authRoute);
app.use('/user', userRoute);
app.use('/product/favorite', productFavoriteRoute);
app.use('/product/cart', productCartRoute);
app.use('/product', productRoute);
app.use('/order', orderRoute);

app.use(errorHandler);
MainDataSource.initialize()
  .then(() => {
    return app.listen(PORT, () => {
      loggerHelper.info(`Server listening at http://localhost:${PORT}`);
    });
  })
  .catch((e: Error) => {
    loggerHelper.error('Failed to connect to databae, Goodbye', e);
    process.exit(1);
  });
