import { Column, Entity, JoinTable, ManyToMany, ManyToOne } from 'typeorm';
import BaseEntity from '@root/src/utils/base.entity';
import UserEntity from '@root/src/user/entities/user.entity';
import ProductEntity from '@root/src/product/entities/product.entity';

@Entity({ name: 'order', schema: 'order' })
export default class OrderEntity extends BaseEntity {
  @Column({ default: '' })
  location: string;

  @Column({ type: 'json' })
  coordinates: { lat: string; long: string };

  @Column({ default: false })
  status: boolean;

  @Column({ default: 0 })
  fullPrice: number;

  @ManyToOne(() => UserEntity, (user) => user.orders)
  user: UserEntity;

  @ManyToMany(() => ProductEntity)
  @JoinTable({
    schema: 'order',
    name: 'ordersProducts',
    joinColumn: {
      name: 'orderId',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'productId',
      referencedColumnName: 'id'
    }
  })
  products: ProductEntity[];
}
