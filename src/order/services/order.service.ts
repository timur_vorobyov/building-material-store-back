import MainDataSource from '@root/src/utils/main-data-source';
import OrderEntity from '../entities/order.entity';
import { QueryOrderDto } from '../dto/query-order.dto';

export class OrderService {
  private repo;

  constructor() {
    this.repo = MainDataSource.getRepository(OrderEntity);
  }

  public async findAll(params: QueryOrderDto) {
    const { status = false, userId } = params;

    return this.repo.find({
      where: { status, user: { id: userId } },
      relations: {
        products: true
      }
    });
  }
}
