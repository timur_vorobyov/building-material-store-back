import { QueryOrderDto } from '@root/src/order/dto/query-order.dto';
import { SuccessResponse } from '@utils/response/success.response';
import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { OrderService } from '../services/order.service';

export default class OrderController {
  private orderService;

  constructor() {
    this.orderService = new OrderService();
  }

  async findAll(
    request: Request<
      Record<string, never>,
      Record<string, never>,
      Record<string, never>,
      QueryOrderDto
    >,
    response: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.orderService.findAll(request.query);

      return response.send(SuccessResponse({ data }));
    } catch (e) {
      next(e);
    }
  }
}
