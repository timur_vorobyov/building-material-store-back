import { BaseQueryDto } from '@root/src/utils/dto/base-query.dto';

export interface QueryOrderDto extends BaseQueryDto {
  status: boolean;
  userId: number;
}
