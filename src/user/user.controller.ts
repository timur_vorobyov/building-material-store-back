import { NextFunction } from 'express';
import { Request, Response } from 'express-serve-static-core';
import { UserService } from '../user/user.service';
import { SuccessResponse } from '@utils/response/success.response';
import { CreateUserDto } from '../user/dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

export default class UserController {
  private userService;

  constructor() {
    this.userService = new UserService();
  }

  async create(
    request: Request<Record<string, never>, Record<string, never>, CreateUserDto>,
    response: Response,
    next: NextFunction
  ) {
    try {
      await this.userService.create(request.body);

      return response.send(SuccessResponse({ data: 'USER_IS_CREATED' }));
    } catch (e) {
      next(e);
    }
  }

  async update(
    request: Request<Record<string, never>, Record<string, never>, UpdateUserDto>,
    response: Response,
    next: NextFunction
  ) {
    try {
      await this.userService.update(request.params.id, request.body);

      return response.send(SuccessResponse({ data: 'USER_IS_UPDATED' }));
    } catch (e) {
      next(e);
    }
  }

  async remove(
    request: Request<{ id: number }, Record<string, never>, Record<string, never>>,
    response: Response,
    next: NextFunction
  ) {
    try {
      await this.userService.remove(request.params.id);

      return response.send(SuccessResponse({ data: 'USER_IS_REMOVED' }));
    } catch (e) {
      next(e);
    }
  }

  async findOneById(
    request: Request<{ id: number }, Record<string, never>, Record<string, never>>,
    response: Response,
    next: NextFunction
  ) {
    try {
      const data = await this.userService.findOneById(request.params.id);
      return response.send(SuccessResponse({ data }));
    } catch (e) {
      next(e);
    }
  }
}
