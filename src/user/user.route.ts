import express from 'express';
import UserController from './user.controller';
const router = express.Router();

const controller = new UserController();
router.post('/', controller.create.bind(controller));
router.put('/:id', controller.update.bind(controller));
router.get('/:id', controller.findOneById.bind(controller));
router.delete('/:id', controller.remove.bind(controller));

export default router;
