import { Entity, Column, OneToMany, ManyToMany, JoinTable } from 'typeorm';
import BaseEntity from '@root/src/utils/base.entity';
import { UserInterface } from '@root/src/user/entities/interfaces/user.interface';
import argon2 from 'argon2';
import ProductEntity from '@root/src/product/entities/product.entity';
import { ProductCartEntity } from '@root/src/product/entities/product-cart.entity';
import OrderEntity from '@root/src/order/entities/order.entity';

@Entity({ name: 'user', schema: 'user' })
export default class UserEntity extends BaseEntity implements UserInterface {
  @Column({ default: '', nullable: false })
  name: string;

  @Column({ default: '', nullable: false })
  phone: string;

  @Column({ default: '', nullable: false })
  password: string;

  @Column({ default: false })
  isAdmin: boolean;

  @OneToMany(() => OrderEntity, (order) => order.user)
  orders: OrderEntity[];

  @OneToMany(() => ProductEntity, (product) => product.user)
  products: ProductEntity[];

  @ManyToMany(() => ProductEntity, (o) => o.favorite)
  @JoinTable({
    schema: 'order',
    name: 'favorite',
    joinColumn: {
      name: 'userId',
      referencedColumnName: 'id'
    },
    inverseJoinColumn: {
      name: 'productId',
      referencedColumnName: 'id'
    }
  })
  favorite: ProductEntity[];

  @OneToMany(() => ProductCartEntity, (cart) => cart.user)
  cart: ProductCartEntity[];

  public async comparePassword(password: string): Promise<boolean> {
    return await argon2.verify(this.password, password);
  }
}
