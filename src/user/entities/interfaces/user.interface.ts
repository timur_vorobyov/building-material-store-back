export interface UserInterface {
  name: string;
  phone: string;
  password: string;
  isAdmin: boolean;
}
