import MainDataSource from '@utils/main-data-source';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';
import UserEntity from './entities/user.entity';

export class UserService {
  private repo;

  constructor() {
    this.repo = MainDataSource.getRepository(UserEntity);
  }

  public async create(createUserDto: CreateUserDto) {
    const newUser = this.repo.create(createUserDto);
    return this.repo.save(newUser);
  }

  public async findOneById(id: number) {
    return await this.repo.findOne({
      where: { id },
      relations: {
        favorite: true,
        cart: { product: true }
      }
    });
  }

  public findByPhone(phone: string) {
    return this.repo.findOne({ where: { phone } });
  }

  public async update(id: number, updateUserDto: UpdateUserDto) {
    await this.repo.findOne({ where: { id } });
    return this.repo.save({ id, ...updateUserDto });
  }

  public async remove(id: number) {
    await this.repo.findOne({ where: { id } });
    return this.repo.delete(id);
  }

  public async isExist(phone: string): Promise<boolean> {
    const count = await this.repo.count({ where: { phone } });
    return count > 0;
  }
}
