export interface UpdateUserDto {
  name?: string;
  phone?: string;
  password?: string;
  isAdmin?: boolean;
}
