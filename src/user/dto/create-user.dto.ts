export interface CreateUserDto {
  name: string;
  phone: string;
  password: string;
  isAdmin: boolean;
}
