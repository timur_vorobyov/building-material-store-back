import { NextFunction } from 'express';
import AuthService from '../auth/auth.service';
import UserEntity from '../user/entities/user.entity';
import { ACCESS_TOKEN_COOKIE_NAME } from '../utils/config';
import MainDataSource from '../utils/main-data-source';

export default async function AuthCheckMiddleware(request: any, response: any, next: NextFunction) {
  const cookieToken = request.cookies[ACCESS_TOKEN_COOKIE_NAME];
  const token = cookieToken || request.headers['auth'];
  const authService = new AuthService();
  if (!token) return undefined;

  try {
    const jwtPayload = await authService.validateAccessToken(token);
    return await MainDataSource.getRepository(UserEntity).findOne({
      where: { id: jwtPayload.id }
    });
  } catch (e) {
    return undefined;
  }
}
