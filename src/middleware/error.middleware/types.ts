/* eslint-disable @typescript-eslint/no-explicit-any*/
export interface ExpressErrorMiddlewareInterface {
  error(error: any, request: any, response: any, next: (err?: any) => any): void;
}

export class HttpError extends Error {
  httpCode: number;
  constructor(httpCode: number, message?: string) {
    super();
    this.httpCode = httpCode;
    this.message = message ?? '';
  }
}
