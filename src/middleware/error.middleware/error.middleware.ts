/* eslint-disable @typescript-eslint/no-explicit-any*/
import { NODE_ENV } from '@root/src/utils/config';
import loggerHelper from '@root/src/utils/helpers/logger.helper';
import { ForbiddenResponse } from '@root/src/utils/response/forbidden.response';
import { NotFoundErrorResponse } from '@root/src/utils/response/not-found.response';
import { UnauthorizedResponse } from '@root/src/utils/response/unauthorized.response';
import { ValidatorErrorResponse } from '@root/src/utils/response/validation-error.response';
import { EntityMetadataNotFoundError, EntityNotFoundError, QueryFailedError } from 'typeorm';
import { HttpError } from './types';

export default function errorHandler(
  error: any,
  request: any,
  response: any
  //next: (err?: any) => any
): void {
  let responseObject = {
    status: 'error'
  } as any;

  console.log(error);
  // BadRequestError
  if (error.constructor.name == 'QueryFailedError') {
    responseObject.message = 'Internal server error';
    response.code = 500;
  }

  if (
    error.constructor.name === 'AuthorizationRequiredError' ||
    error.constructor.name === 'UnauthorizedError'
  ) {
    responseObject.message = error.message.startsWith('Authorization is required for')
      ? 'Unauthorized'
      : error.message;
    responseObject.name = undefined;
    responseObject.stack = undefined;
    responseObject.code = 401;
  }

  //if it's an array of ValidationError
  if (error instanceof ValidatorErrorResponse) {
    responseObject = {
      ...error,
      code: 422,
      status: 'error',
      httpCode: 422,
      name: 'ValidatorErrorResponse'
    };
    responseObject.message = "You have an error in your request's body.";
  } else if (error.errors && error.errors.length) {
    responseObject = {
      data: error.errors || error.data,
      message: "You have an error in your request's body",
      code: 422,
      status: 'error',
      httpCode: 422,
      name: 'ValidatorErrorResponse'
    };
  } else {
    //set http status
    if (error instanceof HttpError && error.httpCode) {
      responseObject.code = error.httpCode;
    } else if (error instanceof UnauthorizedResponse || error instanceof ForbiddenResponse) {
      responseObject.code = error.code;
      responseObject.message = error.message;
      responseObject.data = error.data;
    } else if (
      error instanceof NotFoundErrorResponse ||
      error instanceof EntityNotFoundError ||
      error instanceof EntityMetadataNotFoundError
    ) {
      response.code = 404;
      responseObject.message = 'Not found';
    } else if (error instanceof QueryFailedError || error instanceof TypeError) {
      response.code = 500;
      response.message = 'Internal server error';
    } else if (error.constructor.name === 'PayloadTooLargeError') {
      response.code = 500;
      response.message = 'Request is too large';
    } else {
      responseObject.code = 500;
    }
  }

  if (error instanceof Error) {
    const developmentMode: boolean = NODE_ENV !== 'production';

    //set response error fields
    if (error.name && developmentMode && error.message) {
      //show name only if in development mode and if error message exist too
      responseObject.name = error.name;
    }
    if (error.stack && developmentMode) {
      responseObject.stack = error.stack;
    }
  } else if (typeof error === 'string') {
    responseObject.message = error;
  }

  loggerHelper.error(error);

  return response.status(responseObject.code).json(responseObject);
}
